<?php 
session_start();
session_destroy();
header("location:index.php?logout");
exit;
?>
<html>
<head>
<title>Rascal Dog litter box Admin Panel</title>
<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" rightmargin="0" bottommargin="0">
<center>
<Table cellpadding="0" cellspacing="0" width="100%" height="100%" border="0">
	<tr>
		<td align="left" valign="top" height="113" background="images/logo_bg.gif">
			<Table cellpadding="0" cellspacing="0" width="100%" border="0">
				<tr>
					<!--td align="left" valign="top"><logo><img src="images/ae_logo.gif" hspace="0" vspace="0" border="0" align="top"></td-->
					<td align="right" valign="top"><img src="images/logo_right.gif" hspace="0" vspace="0" border="0" align="top"></td>
				</tr>
			</Table>
		</td>
	</tr>
	<tr>
		<Td align="center" valign="middle" bgcolor="#f8f1e7">
			<table cellpadding="0" cellspacing="0" width="378" border="0" height="190" align="center" valign="middle">
	<tr>
		<td align="left" valign="top" height="100%">
			<Table cellpadding="0" cellspacing="0" width="100%" height="100%" border="1" bordercolor="#186db0" style="border-collapse:collapse">
				<tr>
					<td align="left" valign="top" height="31" background="images/title_bg.jpg" style="background-position:center">
						<Table cellpadding="0" cellspacing="0" width="100%" border="0">
							<tr class="ntitle">
								<Td align="left" valign="top" width="25"><img src="images/bullet1.gif" hspace="0" vspace="0" border="0" align="top"></Td>
								<td align="left" valign="middle">Logout</td>
							</tr>
						</Table>
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" bgcolor="#fbf8f3">
						<Table cellpadding="0" cellspacing="0" width="100%" border="0">
							<Tr>
								<Td align="left" valign="top" width="133"><img src="images/login.jpg" hspace="0" vspace="0" border="0" align="top"></Td>
								<Td align="left" valign="middle">										  <table width="196" border="0" cellpadding="0" cellspacing="0">
								  <tr class="ntitle">
									<td align="left" valign="top" width="17"><div align="center"><img src="images/btn_left.gif" hspace="0" vspace="0" border="0" align="top"></div></td>
									<td align="center" valign="middle" background="images/btn_bg.gif"><div align="center"><a href="index.php"><font color="#FFFFFF">Click here to Login again.</font></a></div></td>
									<td align="left" valign="top" width="17"><div align="center"><img src="images/btn_right.gif" hspace="0" vspace="0" border="0" align="top"></div></td>
								  </tr>
								</table></Td>
								<Td align="left" valign="top" width="16"><img src="images/spacer.gif" hspace="0" vspace="0" border="0" align="top" width="16" height="1"></Td>
							</Tr>
						</Table>
					</td>
				</tr>
				<tr class="blue11">
					<td align="center" valign="middle" bgcolor="#daecf2" height="27"><strong>You have successfully logged out.</strong></td>
				</tr>
			</table>
		</td>
		<td align="left" valign="top" width="3" bgcolor="#e6e0d7"><img src="images/shadow_right.gif" hspace="0" vspace="0" border="0" align="top"></td>
	</tr>
	<tr>
		<Td align="left" valign="top" colspan="2" bgcolor="#e6e0d7" height="3"><img src="images/shadow_bottom.gif" hspace="0" vspace="0" border="0" align="top"></Td>
	</tr>
</table>
		</Td>
	</tr>
	<tr>
		<td align="left" valign="top" height="2" background="images/2verlines.gif"><img src="images/spacer.gif" hspace="0" vspace="0" border="0" align="top" height="2" width="1"></td>
	</tr>
	<tr class="copyright">
		<td align="center" valign="middle" height="31" bgcolor="#3d3d3d">Copyright &copy; 2007.</td>
	</tr>
</Table>
</center>
</body>