<?php 
	require_once "../config.php";
	
	$loop = $_GET["key"] ;
	$str = "<table><tr><td>";
	$faculties = getComboBox("tbl_faculty"," * ",0 );
	$venue = getComboBox("tbl_venue_master"," * ",0 );
	$sem = "<option value='8'>8</option>
			<option value='7'>7</option>
			<option value='6'>6</option>
			<option value='5'>5</option>
			<option value='4'>4</option>
			<option value='3'>3</option>
			<option value='2'>2</option>
			<option value='1'>1</option>";
			
						
	for ($i=1;$i<=$loop;$i++){
		$str .= "
		<table class='new-tab'>
				<input type='hidden' name='".$i."' id='".$i."' value='0'>
				<tr>
					<td colspan='4'><b>Day ".$i." </b><span><input type='button' onclick='addpanel(".$i.")' value='Add'></span></td>
				</tr>
				<tr>
					<td>
						<table class='new-tab' id='".$i."_1' style='display:none;'>
							<tr> 
								<td><select  class='input4' name='".$i."_1_slottype' onchange='change_slot_content(".$i.",1,this.value);'>
									<option value=''>Select</option>
									<option value='lab'>LAB</option><option value='lecture'>Lecture</option>
									</select></td><td>
									<select class='input4' name='".$i."_1_lecture' id='".$i."_1_lecture' style='display:none;'>
										<option value=''>Select</option>
										<option value='1'>1</option>
										<option value='2'>2</option>
										<option value='2'>3</option>
										<option value='4'>4</option>
										<option value='5'>5</option>
										<option value='6'>6</option>
									</select>
									<select name='".$i."_1_lab' class='input4' id='".$i."_1_lab'>
										<option value=''>Select</option>
										<option value='1-2'>1-2</option>
										<option value='3-4'>3-4</option>
										<option value='5-6'>5-6</option>
									</select>
								</td>	
								<td>Subject</td><td><input type='text' name='".$i."_1_subject'> </td>
								<td>Sem</td><td><select class='input4' name='".$i."_1_sem'>
								<option value=''>Select</option>
								".$sem."</select></td>
								<td><input type='button' value='Hide' onclick='hideit(\"".$i."_1\",".$i.")'></td>
							</tr>
							<tr>					
								<td colspan='2'></td>
								<td>Assign Load to</td>
								<td><select class='input3' name='".$i."_1_load_to'>
								<option value=''>Select</option>
								".$faculties."</select></td>
								<td>Venue</td><td><select class='input4' name='".$i."_1_venue'>
								<option value=''>Select</option>
								".$venue."</select></td>
							</tr>
						</table>
						<table class='new-tab' id='".$i."_2' style='display:none;'>
							<tr> 
								<td><select  class='input4' name='".$i."_2_slottype' onchange='change_slot_content(".$i.",2,this.value);'>
								<option value=''>Select</option>
								<option value='lab'>LAB</option><option value='lecture'>Lecture</option>
									</select></td><td>
									<select class='input4' name='".$i."_2_lecture' id='".$i."_2_lecture' style='display:none;'>
										<option value=''>Select</option>
										<option value='1'>1</option>
										<option value='2'>2</option>
										<option value='2'>3</option>
										<option value='4'>4</option>
										<option value='5'>5</option>
										<option value='6'>6</option>
									</select>
									<select name='".$i."_2_lab' class='input4' id='".$i."_2_lab'>
										<option value=''>Select</option>
										<option value='1-2'>1-2</option>
										<option value='3-4'>3-4</option>
										<option value='5-6'>5-6</option>
									</select>
								</td>	
								<td>Subject</td><td><input type='text' name='".$i."_2_subject'> </td>
								<td>Sem</td><td><select class='input4' name='".$i."_2_sem'><option value=''>Select</option>".$sem."</select></td>
								<td><input type='button' value='Hide' onclick='hideit(\"".$i."_2\",".$i.")'></td>
							</tr>
							<tr>					
								<td colspan='2'></td>
								<td>Assign Load to</td><td><select class='input3' name='".$i."_2_load_to'><option value=''>Select</option>".$faculties."</select></td>
								<td>Venue</td><td><select class='input4' name='".$i."_2_venue'><option value=''>Select</option>".$venue."</select></td>
							</tr>
						</table>
						
						
						<table class='new-tab' id='".$i."_3' style='display:none;'>
							<tr> 
								<td><select  class='input4' name='".$i."_3_slottype' onchange='change_slot_content(".$i.",3,this.value);'><option value=''>Select</option><option value='lab'>LAB</option><option value='lecture'>Lecture</option>
									</select></td><td>
									<select class='input4' name='".$i."_3_lecture' id='".$i."_3_lecture' style='display:none;'>
										<option value=''>Select</option>
										<option value='1'>1</option>
										<option value='2'>2</option>
										<option value='2'>3</option>
										<option value='4'>4</option>
										<option value='5'>5</option>
										<option value='6'>6</option>
									</select>
									<select name='".$i."_3_lab' class='input4' id='".$i."_3_lab'>
										<option value=''>Select</option>
										<option value='1-2'>1-2</option>
										<option value='3-4'>3-4</option>
										<option value='5-6'>5-6</option>
									</select>
								</td>	
								<td>Subject</td><td><input type='text' name='".$i."_3_subject'> </td>
								<td>Sem</td><td><select class='input4' name='".$i."_3_sem'><option value=''>Select</option>".$sem."</select></td>
								<td><input type='button' value='Hide' onclick='hideit(\"".$i."_3\",".$i.")'></td>
							</tr>
							<tr>					
								<td colspan='2'></td>
								<td>Assign Load to</td><td><select class='input3' name='".$i."_3_load_to'><option value=''>Select</option>".$faculties."</select></td>
								<td>Venue</td><td><select class='input4' name='".$i."_3_venue'><option value=''>Select</option>".$venue."</select></td>
							</tr>
						</table>
						<table class='new-tab' id='".$i."_4' style='display:none;'>
							<tr> 
								<td><select  class='input4' name='".$i."_4_slottype' onchange='change_slot_content(".$i.",4,this.value);'><option value=''>Select</option><option value='lab'>LAB</option><option value='lecture'>Lecture</option>
									</select></td><td>
									<select class='input4' name='".$i."_4_lecture' id='".$i."_4_lecture' style='display:none;'>
										<option value=''>Select</option>
										<option value='1'>1</option>
										<option value='2'>2</option>
										<option value='2'>3</option>
										<option value='4'>4</option>
										<option value='5'>5</option>
										<option value='6'>6</option>
									</select>
									<select name='".$i."_4_lab' class='input4' id='".$i."_4_lab'>
										<option value=''>Select</option>
										<option value='1-2'>1-2</option>
										<option value='3-4'>3-4</option>
										<option value='5-6'>5-6</option>
									</select>
								</td>	
								<td>Subject</td><td><input type='text' name='".$i."_4_subject'> </td>
								<td>Sem</td><td><select class='input4' name='".$i."_4_sem'><option value=''>Select</option>".$sem."</select></td>
								<td><input type='button' value='Hide' onclick='hideit(\"".$i."_4\",".$i.")'></td>
							</tr>
							<tr>					
								<td colspan='2'></td>
								<td>Assign Load to</td><td><select class='input3' name='".$i."_4_load_to'><option value=''>Select</option>".$faculties."</select></td>
								<td>Venue</td><td><select class='input4' name='".$i."_4_venue'><option value=''>Select</option>".$venue."</select></td>
							</tr>
						</table>
						<table class='new-tab' id='".$i."_5' style='display:none;'>
							<tr> 
								<td><select  class='input4' name='".$i."_5_slottype' onchange='change_slot_content(".$i.",5,this.value);'><option value=''>Select</option><option value='lab'>LAB</option><option value='lecture'>Lecture</option>
									</select></td><td>
									<select class='input4' name='".$i."_5_lecture' id='".$i."_5_lecture' style='display:none;'>
										<option value=''>Select</option>
										<option value='1'>1</option>
										<option value='2'>2</option>
										<option value='2'>3</option>
										<option value='4'>4</option>
										<option value='5'>5</option>
										<option value='6'>6</option>
									</select>
									<select name='".$i."_5_lab' class='input4' id='".$i."_5_lab'>
										<option value=''>Select</option>
										<option value='1-2'>1-2</option>
										<option value='3-4'>3-4</option>
										<option value='5-6'>5-6</option>
									</select>
								</td>	
								<td>Subject</td><td><input type='text' name='".$i."_5_subject'> </td>
								<td>Sem</td><td><select class='input4' name='".$i."_5_sem'><option value=''>Select</option>".$sem."</select></td>
								<td><input type='button' value='Hide' onclick='hideit(\"".$i."_5\",".$i.")'></td>
							</tr>
							<tr>					
								<td colspan='2'></td>
								<td>Assign Load to</td><td><select class='input3' name='".$i."_5_load_to'><option value=''>Select</option>".$faculties."</select></td>
								<td>Venue</td><td><select class='input4' name='".$i."_5_venue'><option value=''>Select</option>".$venue."</select></td>
							</tr>
						</table>
						<table class='new-tab' id='".$i."_6' style='display:none;'>
							<tr> 
								<td><select  class='input4' name='".$i."_6_slottype' onchange='change_slot_content(".$i.",6,this.value);'><option value=''>Select</option><option value='lab'>LAB</option><option value='lecture'>Lecture</option>
									</select></td><td>
									<select class='input4' name='".$i."_6_lecture' id='".$i."_6_lecture' style='display:none;'>
										<option value=''>Select</option>
										<option value='1'>1</option>
										<option value='2'>2</option>
										<option value='2'>3</option>
										<option value='4'>4</option>
										<option value='5'>5</option>
										<option value='6'>6</option>
									</select>
									<select name='".$i."_6_lab' class='input4' id='".$i."_6_lab'>
										<option value=''>Select</option>
										<option value='1-2'>1-2</option>
										<option value='3-4'>3-4</option>
										<option value='5-6'>5-6</option>
									</select>
								</td>	
								<td>Subject</td><td><input type='text' name='".$i."_6_subject'> </td>
								<td>Sem</td><td><select class='input4' name='".$i."_6_sem'><option value=''>Select</option>".$sem."</select></td>
								<td><input type='button' value='Hide' onclick='hideit(\"".$i."_6\",".$i.")'></td>
							</tr>
							<tr>					
								<td colspan='2'></td>
								<td>Assign Load to</td><td><select class='input3' name='".$i."_6_load_to'><option value=''>Select</option>".$faculties."</select></td>
								<td>Venue</td><td><select class='input4' name='".$i."_6_venue'><option value=''>Select</option>".$venue."</select></td>
							</tr>
						</table>
					</td>
				</tr>
				</table>";
	}
	$str.="</td></tr></table>";
	echo $str;	
?>
