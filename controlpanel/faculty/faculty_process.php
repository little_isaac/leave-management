<?php

include_once(ADMIN_PATH."include/functions.php");
require_once(ADMIN_PATH."include/img_upload.php");
if (!isAdminLoggedIn())
	header("location:index.php");
else	
	$adminid=isAdminLoggedIn();

$errMsg=array();
$errcnt=0;
$image_modify;
$id = isset($_GET["id"])?($_GET["id"]):0;
	if((isset($_GET["id"])) && ($_GET["id"]!="")){
		$staff_id = isset($_GET["id"])?($_GET["id"]):0;
		$sql =mysql_query(" select * from tbl_faculty where staff_id = ".$staff_id."");
		$result =mysql_fetch_assoc($sql);
		
		$staff_id = $result["staff_id"];
		
		$first_name        =$result["first_name"];
		$last_name         =$result["last_name"];
		$middle_name       =$result["middle_name"];
		$staff_login_id    =$result["staff_login_id"];
		$contact_1         =$result["contact_1"];
		$contact_2         =$result["contact_2"];
		$alternative_email =$result["alternative_email"];
		$password          =$result["password"];
		$address           =$result["address"];
		$category          =$result["category"];
		$dob               =ymdtodmy($result["dob"]);
		$doj               =ymdtodmy($result["doj"]);
		$gender            =$result["gender"];
		$dept_id		   =$result["dept_id"];
		$desig_id		   =$result["designation_id"];
		$city_id           =$result["city_id"];
		
		$image= $result["image"];
		$image_modify=$image;
		
	}
	$addRqst = isset($_POST["add_x"])?TRUE:FALSE;
    $updateRqst = isset($_POST["update_x"])?TRUE:FALSE;
	if($addRqst || $updateRqst){
		
		$first_name=isset($_POST["first_name"])?stripslashes($_POST["first_name"]):"";
		$middle_name=isset($_POST["middle_name"])?stripslashes($_POST["middle_name"]):"";
		$last_name=isset($_POST["last_name"])?stripslashes($_POST["last_name"]):"";
		$staff_login_id=isset($_POST["staff_login_id"])?stripslashes($_POST["staff_login_id"]):"";
		$contact_1=isset($_POST["contact_1"])?stripslashes($_POST["contact_1"]):"";
		$contact_2=isset($_POST["contact_2"])?stripslashes($_POST["contact_2"]):"";
		$alternative_email=isset($_POST["alternative_email"])?stripslashes($_POST["alternative_email"]):"";
		$password=isset($_POST["password"])?stripslashes($_POST["password"]):"";
		$address=isset($_POST["address"])?stripslashes($_POST["address"]):"";
		$category=isset($_POST["category"])?stripslashes($_POST["category"]):"";
		$dept_id=isset($_POST["dept_id"])?stripslashes($_POST["dept_id"]):"";
		$city_id=isset($_POST["city_id"])?stripslashes($_POST["city_id"]):"";
		$dob=isset($_POST["dob"])?stripslashes($_POST["dob"]):"";
		$doj=isset($_POST["doj"])?stripslashes($_POST["doj"]):"";
		$desig_id=isset($_POST["desig_id"])?stripslashes($_POST["desig_id"]):"";
		$gender=isset($_POST["gender"])?stripslashes($_POST["gender"]):"";
		$image =isset($_FILES["file"]["name"]) ? stripslashes($_FILES["file"]["name"]): "";
		
		//$date =date("Y-m-d H:i:s");
		if($dob!="")
			$dob = dmytoymd($dob);
		if($doj!="")
			$doj = dmytoymd($doj);
		
		//validation
		
		$style="";
		
		if(empty($first_name)){
			$errMsg= "error";
			$first_style="style='border:solid 1px #ff0000; '";
			$err_fname=" Please enter First Name\n";
		}
		else if (!preg_match("/^[a-zA-Z]*$/",$first_name))
		{
			$errMsg= "error";
			$first_style="style='border:solid 1px #ff0000; '";
			$err_fname=" Please enter Proper First Name\n";
		}
		if(empty($middle_name)){
			$errMsg= "error";
			$middle_style="style='border:solid 1px #ff0000; '";
			$err_mname=" Please enter Middle Name\n";
		}
		else if (!preg_match("/^[a-zA-Z]*$/",$middle_name))
		{
			$errMsg= "error";
			$middle_style="style='border:solid 1px #ff0000; '";
			$err_mname=" Please enter Proper Middle Name\n";
		}
		
		if(empty($last_name)){
			$errMsg= "error";
			$last_style="style='border:solid 1px #ff0000; '";
			$err_lname=" Please enter Last Name\n";
		}
		else if (!preg_match("/^[a-zA-Z]*$/",$last_name))
		{
			$errMsg= "error";
			$last_style="style='border:solid 1px #ff0000; '";
			$err_lname=" Please enter Proper Last Name\n";
		}
		if(empty($staff_login_id)){
			$errMsg= "error";
			$staff_login_id_style="style='border:solid 1px #ff0000; '";
			$err_staff_login_id=" Please enter Staff Login ID\n";
		}
		else if (!filter_var($staff_login_id, FILTER_VALIDATE_EMAIL))
		{
			$errMsg= "error";
			$staff_login_id_style="style='border:solid 1px #ff0000; '";
			$err_staff_login_id=" Please enter Proper Login Id\n";
		}
		if(empty($contact_1)){
			$errMsg= "error";
			$contact_1_style="style='border:solid 1px #ff0000; '";
			$err_contact_1=" Please enter contact - 1 detail\n";
		}
		else if (!preg_match("/^[0-9]{10}$/",$contact_1))
		{
			$errMsg= "error";
			$contact_1_style="style='border:solid 1px #ff0000; '";
			$err_contact_1=" Please enter Proper contact_1 detail\n";
		}
		if(!empty($contact_2)){
			if (!preg_match("/^[0-9]{10}$/",$contact_2))
			{
				$errMsg= "error";
				$contact_1_style="style='border:solid 1px #ff0000; '";
				$err_contact_1=" Please enter Proper contact_1 detail\n";
			}
		}
		else
		{
			$contact_2 = "";
		}
		if(!empty($alternative_email)){
			if (!filter_var($alternative_email, FILTER_VALIDATE_EMAIL))
			{
				$errMsg= "error";
				$alternative_style="style='border:solid 1px #ff0000; '";
				$err_alternative=" Please enter Proper Login Id\n";
			}
		}
		else
		{
			$alternative_email = "";
		}
		if(empty($password)){
			$errMsg= "error";
			$password_style="style='border:solid 1px #ff0000; '";
			$err_password=" Please enter password\n";
		}
		if(empty($address)){
			$errMsg= "error";
			$address_style="style='border:solid 1px #ff0000; '";
			$err_address=" Please enter address\n";
		}
		if(empty($category)){
			$errMsg= "error";
			$category_style="style='border:solid 1px #ff0000; '";
			$err_category=" Please enter Your category\n";
		}
		if(empty($dept_id)){
			$errMsg= "error";
			$dept_style="style='border:solid 1px #ff0000; '";
			$err_dept=" Please select Your department\n";
		}
		if(empty($city_id)){
			$errMsg= "error";
			$city_style="style='border:solid 1px #ff0000; '";
			$err_city=" Please enter your current City\n";
		}
				
		if(empty($dob)){
			$errMsg= "error";
			$dob_style="style='border:solid 1px #ff0000; '";
			$err_dob=" Please enter Date Of Birth\n";
		}
		if(empty($doj)){
			$errMsg= "error";
			$doj_style="style='border:solid 1px #ff0000; '";
			$err_doj=" Please enter Date Of Joining0\n";
		}
		if(empty($desig_id)){
			$errMsg= "error";
			$desig_style="style='border:solid 1px #ff0000; '";
			$err_desig=" Please select your designation\n";
		}
		
		if(empty($gender)){
			$errMsg= "error";
			$gender_style="style='border:solid 1px #ff0000; '";
			$err_gender=" Please Select your gender\n";
		}

			
		if($image!=""){
			if($image=="" && ($id==0)){
					$errMsg= "error";
						$image_style="style='border:solid 1px #ff0000; '";
						$err_image="Please upload image.\n";

				}
				if($image!="") {
				$n1=strripos($image,".");
				$v1 =substr($image,$n1);
				$v1 = strtolower($v1);
				if(($v1=='.jpg') || ($v1=='.png') || ($v1=='.jpeg') || ($v1=='.gif')  ){}
				else {	
					$errMsg= "error";
					$image_style="style='border:solid 1px #ff0000; '";
					$err_image="Only JPG/JPEG/GIF/PNG type image files are allowed.Please select a valid file type.\n";
				}
			}
		}
		else
		{	if(!isset($_GET['id']))
					{
				$errMsg= "error";
				$image_style="style='border:solid 1px #ff0000; '";
				$err_image="Please upload image.\n";
				}
				else{ 	}
		}
		
		 
			
        // image upload code in product images folder
		if($_FILES["file"]["name"]!=""){
			$err = "";
			$img_upload = new img_upload(PROFILE_IMAGE_PATH,"file");
			$img_upload->setThumb("th-",100,100);
			$fileName = $img_upload->process($err);
		}
		
		if($staff_id!=0)
                $and .= " AND staff_id<>".$staff_id;
            $qry1 = "SELECT staff_id FROM  tbl_faculty WHERE staff_login_id='".addslashes($staff_login_id)."'".$and;
            $rs1 = $db->Query($qry1);
            if(mysql_num_rows($rs1) > 0) {
                $errMsg= "error";
				$staff_login_id_style="style='border:solid 1px #ff0000; '";
				$err_staff_login_id="already exist please enter another Login ID\n";
            }
	
		if($addRqst){
		
			if(empty($errMsg))
			{
				
				$data =array("staff_login_id"=>$staff_login_id,"first_name"=>$first_name,"middle_name"=>$middle_name,"last_name"=>$last_name,"contact_1"=>$contact_1,"contact_2"=>$contact_2,"alternative_email"=>$alternative_email,"password"=>$password,"address"=>$address,"category"=>$category,"dept_id"=>$dept_id,"city_id"=>$city_id,"dob"=>$dob,"doj"=>$doj,"designation_id"=>$desig_id,"gender"=>$gender,"image"=>$fileName);
				
		
				if($db->Insert($data,"tbl_Faculty")){
					header("location:main.php?pg=viewfaculty&msg");
					exit;
				}
	
			
			}			
		}	
		if($updateRqst){
			if(empty($errMsg)){
				$data =array("staff_login_id"=>$staff_login_id,"first_name"=>$first_name,"middle_name"=>$middle_name,"last_name"=>$last_name,"contact_1"=>$contact_1,"contact_2"=>$contact_2,"alternative_email"=>$alternative_email,"password"=>$password,"address"=>$address,"category"=>$category,"dept_id"=>$dept_id,"city_id"=>$city_id,"dob"=>$dob,"doj"=>$doj,"designation_id"=>$desig_id,"gender"=>$gender);
				//print_r($data);die;
				if($_FILES["file"]["name"]!=""){
					$data = array_merge($data,array("image"=>$fileName));
				}
				if($db->Update($data,"tbl_Faculty"," staff_id=".$staff_id."")){
					header("location:main.php?pg=viewfaculty&smsg");
					exit;
				}
			}		
		}
		
	}
?>
<script language="javascript" src="javascript/validation.js"></script>
 <link rel="stylesheet" href="css/jquery-ui.css">
<script src="javascript/jquery-1.10.2.js"></script>
<script src="javascript/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="css/style.css">
 <script>
$(function() {
$( "#dob" ).datepicker();
});
$(function() {
$( "#doj" ).datepicker();
});
</script>
<style>
body {
	font-family: "Trebuchet MS", "Helvetica", "Arial",  "Verdana", "sans-serif";
	font-size: 62.5%;
}
</style>
<script>

function validate()
{
		$first_name = document.getElementsByName("first_name").value;
		$middle_name = document.getElementsByName("middle_name").value;
		$last_name = document.getElementsByName("last_name").value;
		$staff_login_id = document.getElementsByName("staff_login_id").value;
		$contact_1 = document.getElementsByName("contact_1").value;
		$contact_2 = document.getElementsByName("contact_2").value;
		$alternative_email = document.getElementsByName("alternative_email").value;
		$password = document.getElementsByName("password").value;
		$address = document.getElementsByName("address").value;
		$category = document.getElementsByName("category").value;
		$dept_name = document.getElementsByName("dept_name").value;
		$city_name = document.getElementsByName("city_name").value;
		$dob = document.getElementsByName("dob").value;
		$doj = document.getElementsByName("doj").value;
		$desig_name = document.getElementsByName("desig_name").value;
		$gender = document.getElementsByName("gender").value;
}
 </script>
<link rel="stylesheet" type="text/css" href="css/style.css">
<Table cellpadding="0" cellspacing="0" width="100%" border="0" height="100%">
	<tr>
		<td align="left" valign="top" height="10"><img src="images/spacer.gif" hspace="0" vspace="0" border="0" align="top" height="10" width="8"></td>
	</tr>
	<tr>
		<td align="left" valign="top" width="100%">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td align="left" valign="top" width="10"><img src="images/spacer.gif" hspace="0" vspace="0" border="0" align="top" height="1" width="10"></td>
					<td align="left" valign="top" width="100%">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
									<table>
										<tr>
											<td class="job-td">
												<table class="job-list">
													<tr>
														<td style="padding-left:10px;" class="arial-16"><?php if ($id!=0) echo "Edit Product "; else echo "Add Faculty";?></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>	
												<form name="frmadminadded" method="post" action="" enctype="multipart/form-data">
													<input type="hidden" value="add" name="act" />
												<table class="new-tab">
												
													<tr valign="top"> 
														<td width="100px"> First Name</td>
														<td ><input name="first_name"  class="input3" id="first_name" value="<?php echo $first_name;?>"  maxlength="255"  <?php echo $first_style; ?> > <span ><?php echo $err_fname;?> </span></td>
													
													</tr>
													<tr valign="top"> 
														<td width="100px"> Middle Name</td>
														<td ><input name="middle_name"  class="input3" id="middle_name" value="<?php echo $middle_name;?>"  maxlength="255"  <?php echo $middle_style; ?> > <span ><?php echo $err_mname;?> </span></td>
													
													</tr>
													<tr valign="top"> 
														<td width="100px"> Last Name</td>
														<td ><input name="last_name"  class="input3"  value="<?php echo $last_name;?>"  maxlength="255"  <?php echo $last_style; ?> > <span ><?php echo $err_lname;?> </span></td>
													
													</tr>
													<tr valign="top"> 
														<td width="100px"> Login Id</td>
														<td ><input name="staff_login_id"  class="input3"  value="<?php echo $staff_login_id;?>"  maxlength="255"  <?php echo $staff_login_id_style; ?> > <span ><?php echo $err_staff_login_id;?> </span></td>
													
													</tr>
													
													<tr valign="top"> 
														<td width="100px"> Contact - 1</td>
														<td ><input name="contact_1"  class="input3"  value="<?php echo $contact_1;?>"  maxlength="255"  <?php echo $contact_1_style; ?> > <span ><?php echo $err_contact_1;?> </span></td>
													
													</tr>
													<tr valign="top"> 
														<td width="100px"> Contact - 2</td>
														<td ><input name="contact_2"  class="input3"  value="<?php echo $contact_2;?>"  maxlength="255"  <?php echo $contact_2_style; ?> > <span ><?php echo $err_contact_2;?> </span></td>
													
													</tr>
													<tr valign="top"> 
														<td width="100px"> Alternative email</td>
														<td ><input name="alternative_email"  class="input3"  value="<?php echo $alternative_email;?>"  maxlength="255"  <?php echo $alternative_style; ?> > <span ><?php echo $err_alternative;?> </span></td>
													
													</tr>
													<tr valign="top"> 
														<td width="100px"> Password</td>
														<td ><input name="password"  class="input3" type="password"  value="<?php echo $password;?>"  maxlength="255"  <?php echo $password_style; ?> > <span ><?php echo $err_password;?> </span></td>
													
													</tr>
													<tr valign="top"> 
														<td width="100px"> Address</td>
														<td ><input name="address"  class="input3"  value="<?php echo $address;?>"  maxlength="255"  <?php echo $address_style; ?>> </textarea> <span ><?php echo $err_address;?> </span></td>
													
													</tr>
													<tr valign="top"> 
														<td width="100px"> Category</td>
														<td ><input name="category"  class="input3"  value="<?php echo $category;?>"  maxlength="255"  <?php echo $category_style; ?> > <span ><?php echo $err_category;?> </span></td>
													
													</tr>
													<tr valign="top"> 
														<td width="180px">Department</td>
														<td ><select name="dept_id" class="input3"><?php echo getComboBox("tbl_department_master "," * " ,$dept_id);?> </select><span ><?php echo $err_dept;?> </span></td>
													</tr>
													<tr valign="top"> 
														<td width="180px">City </td>
														<td ><select name="city_id" class="input3" ><?php echo getComboBox("tbl_city_master"," * " ,$city_id);?> </select><span ><?php echo $err_city;?> </span></td>
													</tr>
													<tr valign="top"> 
														<td width="100px"> Date of birth</td>
														<td ><input name="dob" id="dob"  readonly="true" class="input3"  value="<?php echo $dob;?>"  maxlength="255"  <?php echo $dob_style; ?> > <span ><?php echo $err_dob;?> </span></td>
													
													</tr>
													<tr valign="top"> 
														<td width="100px"> Date of Joining</td>
														<td ><input name="doj" id="doj"  readonly="true" class="input3"  value="<?php echo $doj;?>"  maxlength="255"  <?php echo $doj_style; ?> > <span ><?php echo $err_doj;?> </span></td>
													
													</tr>
													
													<tr valign="top"> 
														<td width="180px">Designation </td>
														<td ><select name="desig_id" class="input3" ><?php echo getComboBox("tbl_designation_master"," * " ,$desig_id);?> </select><span ><?php echo $err_design;?> </span></td>
													</tr>
													<tr valign="top"> 
														<td width="100px"> Gender</td>
														<td > <input type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?>  value="male">Male
															  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?>  value="female">Female
															  <span class="error"> <?php echo $err_gender;?></span></td>
													
													</tr>
													
													<tr>	
														<td width="100px">Upload Image</td>
														<td ><input name="file"  class="input3"  value="<?php echo PROFILE_IMAGE_PATH.$image;?>"   type="file"  > <span ><?php echo $err_image;?> </span></td>	
													</tr>	
													
													<?php 
														
														if(($id!=0) && ($image!="") && (file_exists(PROFILE_IMAGE_PATH.$image))){ 
														
														?>
															<tr>	
														<td width="100px">Uploaded Image</td>
														<td> <img src="<?php echo PROFILE_IMAGE_URL."th-".$image;?>"></td>	
													</tr>	
														<? }		
														else if(($id!=0))
														{
															if(isset($_POST['update_x']))
															{ 
															
															?>
														<tr>	
														<td width="100px">Uploaded Image</td>
														<td> <img src="<?php echo PROFILE_IMAGE_URL."th-".$image_modify;?>"></td>	
													</tr>	
																
															<?php
																
															}
														}
													?>
													<tr style="background:#f9f9f9;">
														<td></td>
														<?php
																									  
													  if($id!=0){
														$name = "name='update'";
														}		
													  else{ 	
														$name = "name='add'";	
														}
														$onclick = "onClick=\"return validate()\"";
																												
														?>
														<td style="text-align:left;"><a href="#"><input type="image" <?php echo $name;?> <?php echo $onclick;?> src="images/submit1.gif" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="main.php?pg=viewfaculty"><img src="images/back1.gif" /></a></td>
													</tr>
													</table>
													</form>
												</td>
											</tr>
										</table>
									</td>
								</tr>
						</table>
					</td>
					<td align="left" valign="top" width="15"><img src="images/spacer.gif" hspace="0" vspace="0" border="0" align="top" height="1" width="15"></td>
				</tr>
			</table>
		</td>
	</tr>
</Table>