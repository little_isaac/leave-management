function ask(frm,id,msg) {
	msg = typeof(msg)=="undefined"?"Are you sure you want to delete this entry?":msg;
	
    if(confirm(msg)) {
		frmObj = eval("document."+frm);
		frmObj.delId.value = id;
		frmObj.submit();
    }
    else {
        return false;
    }
}