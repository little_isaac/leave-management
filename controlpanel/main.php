<?php 
	require_once "../config.php";
	require_once "chkadminsess.php";
	$pg=isset($_REQUEST['pg'])?$_REQUEST['pg']:"";

	$adminsessionstr = trim($_SESSION['adminsessionid']);
	$parts = explode(";",$adminsessionstr);
	$adminsessionstrid=$parts[0];
	$mfile = "";

	if(isset($_REQUEST['pg']) && $_REQUEST['pg']!=""){
		
		$pg=$_REQUEST['pg'];
		
		// Include Admin Module File
		if($pg == 'viewAdmin'){
			$mfile = "adminData/viewAdmin.php";
		}
		elseif($pg == 'addAdmin'){
			$mfile = "adminData/addAdmin.php";
		}
		elseif($pg == 'adminProc'){
			$mfile = "adminData/adminProc.php";
		}
		elseif($pg == 'admdel'){
			$mfile = "adminData/deladmin.php";
		}
		elseif($pg == 'modAdmin'){
			$mfile = "adminData/modAdmin.php";
		}

		
		elseif($pg == 'viewfaculty'){ 
			$mfile = "faculty/viewfaculty.php";
		}
		elseif($pg == 'addfaculty' ){
			$mfile = "faculty/faculty_process.php";
		}
		elseif($pg == 'viewdesignation')
		{
			$mfile = "designation/viewdesignation.php";
		}
		elseif($pg == 'adddesignation')
		{
			$mfile = "designation/designation_process.php";
		}
		elseif($pg == 'viewdepartment')
		{
			$mfile = "department/viewdepartment.php";
		}
		elseif($pg == 'adddepartment')
		{
			$mfile = "department/department_process.php";
		}
		elseif($pg == 'viewvenue')
		{
			$mfile = "venue/viewvenue.php";
		}
		elseif($pg == 'addvenue')
		{
			$mfile = "venue/venue_process.php";
		}
		elseif($pg == 'leavetypemaster'){ 
			$mfile = "leavetypemaster/viewleavetype.php";
		}
		elseif($pg == 'addleavetype' ){
			$mfile = "leavetypemaster/leavetype_process.php";
		}
		elseif($pg == 'leavereq'){ 
			$mfile = "leavereq/viewleavereq.php";
		}
		elseif($pg == 'addleavereq' ){
			$mfile = "leavereq/leavereq_process.php";
		}
		elseif($pg == 'holiday'){ 
			$mfile = "holiday/viewholiday.php";
		}
		elseif($pg == 'addholiday' ){
			$mfile = "holiday/holiday_process.php";
		}
		elseif($pg == 'viewleaves'){ 
			$mfile = "leave_approve/view_leave_taken.php";
		}
		elseif($pg == 'leave_approve' ){
			$mfile = "leave_approve/leave_approve_process.php";
		}
		elseif($pg == 'leave_balance')
		{
			$mfile = "leave_balance/leave_balance.php";
		}
		else if($pg == 'addbalance')
		{
			$mfile = "leave_balance/addbalance.php";
		}
		
		
		
		
}	
else{
	$pg = "";
	$mfile = "";
}


//for delete the coockie value
 
if(stristr($pg,"Admin"))
{
	$title="Admin";
	$page=$_COOKIE['page'];
	
	if(isset($_COOKIE["page"]))
	{
		if($title!=$page)
			deletecoockie();
		
	}
	else
		setcookie("page",$title, time()+10000);
		
}
 
?>
<html>
<head>
<title><?php echo TITLE;?></title>
<script language="javascript" src="javascript/validation.js"></script>
<link href="css/main.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="css/jqueryslidemenu.css" />
<!--<script type="text/javascript" src="javascript/jqueryslidemenu.js"></script>
<script type="text/javascript" src="javascript/jquery.min.js"></script>-->




</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<center>
<table border="0" cellpadding="0" cellspacing="0"  style="height:100%;width:1000px;" >
	<tr>
		<td style="padding:0px 12px;">
			<table>	
				<tr>
					<td class="header">
						<?php include_once("inc/top.php"); ?> 
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	<tr>	
		<td style="padding:0px 12px;height:100%;">
			<table style="height:100%;">
				<tr>
					<td class="border">
						<table border="0" cellspacing="0" cellpadding="0"  width="100%" style="vertical-align:top;">
							<tr>
								<td><?php
									if($mfile != "")
									include_once $mfile;
								?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
</table>
</center>
<table>
	<tr>	
		<td >
				<?php include_once("inc/bottom.php"); ?> 
		</td>
	</tr>
</table>
</body>
</html>