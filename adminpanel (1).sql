-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2015 at 06:41 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `adminpanel`
--

-- --------------------------------------------------------

--
-- Table structure for table `holiday`
--

CREATE TABLE IF NOT EXISTS `holiday` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) NOT NULL,
  `date` date NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `holiday`
--

INSERT INTO `holiday` (`id`, `name`, `date`, `active`) VALUES
(1, 'Shivratri', '2015-02-12', '1');

-- --------------------------------------------------------

--
-- Table structure for table `leave_assigned`
--

CREATE TABLE IF NOT EXISTS `leave_assigned` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `load_adjust_id` int(10) NOT NULL,
  `leave_date` date NOT NULL,
  `faculty_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `a_id` int(10) NOT NULL AUTO_INCREMENT,
  `a_name` varchar(255) NOT NULL,
  `a_pwd` varchar(255) NOT NULL,
  `a_status` varchar(255) NOT NULL,
  `a_email` varchar(255) NOT NULL,
  PRIMARY KEY (`a_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`a_id`, `a_name`, `a_pwd`, `a_status`, `a_email`) VALUES
(1, 'admin', 'admin123', '1', 'admin@ldrp.ac.in');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_city_master`
--

CREATE TABLE IF NOT EXISTS `tbl_city_master` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(20) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_city_master`
--

INSERT INTO `tbl_city_master` (`city_id`, `city_name`) VALUES
(3, 'gandhinagar'),
(4, 'Ahmedabad'),
(5, 'bhavnagar'),
(6, 'mehsana'),
(7, 'kalol'),
(8, 'bayad');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_master`
--

CREATE TABLE IF NOT EXISTS `tbl_department_master` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(20) NOT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_department_master`
--

INSERT INTO `tbl_department_master` (`dept_id`, `dept_name`) VALUES
(1, 'CE'),
(5, 'IT'),
(7, 'Mech'),
(8, 'Auto');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_designation_master`
--

CREATE TABLE IF NOT EXISTS `tbl_designation_master` (
  `desig_id` int(11) NOT NULL AUTO_INCREMENT,
  `desig_name` varchar(20) NOT NULL,
  PRIMARY KEY (`desig_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_designation_master`
--

INSERT INTO `tbl_designation_master` (`desig_id`, `desig_name`) VALUES
(1, 'HOD'),
(2, 'faculty'),
(3, 'Lab Assit.'),
(4, 'Admin'),
(5, 'peon1'),
(12, 'Principal');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faculty`
--

CREATE TABLE IF NOT EXISTS `tbl_faculty` (
  `staff_id` int(10) NOT NULL AUTO_INCREMENT,
  `staff_login_id` varchar(30) NOT NULL,
  `first_name` varchar(254) NOT NULL,
  `middle_name` varchar(20) NOT NULL,
  `last_name` varchar(254) NOT NULL,
  `contact_1` varchar(12) NOT NULL,
  `contact_2` varchar(12) DEFAULT NULL,
  `alternative_email` varchar(40) DEFAULT NULL,
  `password` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `category` varchar(10) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `dob` date NOT NULL,
  `doj` date NOT NULL,
  `designation_id` int(11) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `image` varchar(254) NOT NULL,
  `CL` int(11) NOT NULL DEFAULT '6',
  `SL` int(11) NOT NULL DEFAULT '6',
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `tbl_faculty`
--

INSERT INTO `tbl_faculty` (`staff_id`, `staff_login_id`, `first_name`, `middle_name`, `last_name`, `contact_1`, `contact_2`, `alternative_email`, `password`, `address`, `category`, `dept_id`, `city_id`, `dob`, `doj`, `designation_id`, `gender`, `image`, `CL`, `SL`) VALUES
(20, 'vishalbarot@ldrp.ac.in', 'Vishal', 'Girishbhai', 'Barot', '9725697077', '', 'vishal.ldrp@gmail.com', 'vishal', 'ldrp', 'sebc', 1, 3, '2014-06-15', '2015-01-05', 2, 'Male', '98998989707.jpg', 11, 18),
(21, 'pratik_ce@ldrp.ac.in', 'Pratik', 'Modi', 'Bharatbhai', '9898569889', '', 'pratik1031@gmail.com', 'pratik', 'sec 24', 'sebc', 5, 3, '0000-00-00', '0000-00-00', 1, 'male', '90778707525.jpg', 6, 6),
(22, 'bhavik_ce@ldrp.ac.in', 'Bhavik', 'M', 'Patel', '7845784587', '', '', 'bhavik', 'padusma', 'open', 1, 6, '2013-05-06', '2015-02-05', 3, 'male', '74576767975.jpg', 6, 6),
(23, 'jashvant_ce@ldrp.ac.in', 'jashvant', 'R', 'Dave', '8745877854', '', '', 'jashvant', 'gandhinagar', 'open', 5, 3, '2014-10-22', '2015-02-14', 12, 'male', '92554905257.jpg', 6, 6),
(24, 'milind@ldrp.ac.in', 'Milind', 'Khodabhai', 'Patel', '7405487015', NULL, NULL, 'milind', 'Plot no-114/2,sector-7/C,gandhinagar', 'open', 1, 2, '1994-05-20', '2012-10-01', 4, 'male', '98998989707.jpg', 6, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leavetypemaster`
--

CREATE TABLE IF NOT EXISTS `tbl_leavetypemaster` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `leaveName` varchar(30) NOT NULL,
  `max_cont` float NOT NULL,
  `min_cont` float NOT NULL,
  `total_per_year` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_leavetypemaster`
--

INSERT INTO `tbl_leavetypemaster` (`id`, `leaveName`, `max_cont`, `min_cont`, `total_per_year`) VALUES
(6, 'CL', 3, 0.5, 6),
(7, 'SL', 8, 3, 12);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leave_req_transaction`
--

CREATE TABLE IF NOT EXISTS `tbl_leave_req_transaction` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `dept_id` int(10) NOT NULL,
  `staff_id` int(10) NOT NULL,
  `leavetype_id` int(10) NOT NULL,
  `leave_from_date` date NOT NULL,
  `leave_end_date` date NOT NULL,
  `leave_req_date` datetime NOT NULL,
  `total_days` varchar(10) NOT NULL,
  `is_hod_approve` int(10) NOT NULL,
  `is_admin_approve` int(10) NOT NULL,
  `is_princi_approve` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_leave_req_transaction`
--

INSERT INTO `tbl_leave_req_transaction` (`id`, `dept_id`, `staff_id`, `leavetype_id`, `leave_from_date`, `leave_end_date`, `leave_req_date`, `total_days`, `is_hod_approve`, `is_admin_approve`, `is_princi_approve`) VALUES
(2, 1, 20, 6, '2015-07-02', '2015-07-02', '2015-07-02 13:03:51', '1', 1, 0, 1),
(3, 1, 22, 6, '2015-07-02', '2015-07-02', '2015-07-02 14:42:49', '1', 0, 0, 0),
(4, 1, 20, 6, '2015-07-02', '2015-07-02', '2015-07-02 13:10:45', '1', 0, 0, 0),
(6, 1, 20, 6, '2015-07-04', '2015-07-04', '2015-07-04 07:27:01', '1', 0, 0, 0),
(7, 1, 20, 7, '2015-07-04', '2015-07-04', '2015-07-04 07:27:22', '1', 0, 0, 0),
(8, 1, 20, 6, '2015-07-02', '2015-07-02', '2015-07-02 13:11:07', '1', 0, 0, 0),
(9, 1, 22, 6, '2015-07-02', '2015-07-02', '2015-07-02 13:20:45', '1', 0, 0, 0),
(10, 1, 22, 7, '2015-07-02', '2015-07-02', '2015-07-02 13:22:00', '1', 0, 0, 0),
(11, 1, 22, 6, '2015-07-02', '2015-07-02', '2015-07-02 13:23:23', '1', 0, 0, 0),
(12, 1, 20, 6, '2015-07-02', '2015-07-02', '2015-07-02 14:07:37', '1', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_load_adjustment`
--

CREATE TABLE IF NOT EXISTS `tbl_load_adjustment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `leave_req_id` int(10) NOT NULL,
  `slot_type` varchar(9) NOT NULL,
  `slot` varchar(5) NOT NULL,
  `is_approved_faculty` int(10) NOT NULL,
  `sem` int(10) NOT NULL,
  `sub` varchar(20) NOT NULL,
  `dept_id` int(10) NOT NULL,
  `venue_id` int(10) NOT NULL,
  `faculty_id` int(10) NOT NULL,
  `leave_date` date NOT NULL,
  `from_faculty` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_load_adjustment`
--

INSERT INTO `tbl_load_adjustment` (`id`, `leave_req_id`, `slot_type`, `slot`, `is_approved_faculty`, `sem`, `sub`, `dept_id`, `venue_id`, `faculty_id`, `leave_date`, `from_faculty`) VALUES
(4, 2, '2', '1', 1, 1, 'BE', 1, 5, 22, '2015-07-01', 20),
(5, 3, '1', '12', 1, 1, 'CE', 1, 5, 22, '2015-07-02', 20),
(6, 4, '1', '12', 1, 1, 'CE', 1, 5, 22, '2015-07-04', 20),
(7, 6, '1', '12', 1, 1, 'CE', 1, 5, 22, '2015-07-04', 20),
(8, 7, '1', '12', 1, 1, 'ME', 1, 5, 22, '2015-07-02', 20),
(9, 8, '1', '12', 1, 1, 'EE', 1, 5, 22, '2015-07-02', 20),
(10, 9, '1', '12', 1, 1, 'CE', 1, 5, 20, '2015-07-02', 22),
(11, 10, '2', '1', 1, 1, 'CE', 1, 5, 20, '2015-07-02', 22),
(12, 11, '1', '1', 1, 1, 'CE', 1, 5, 20, '2015-07-02', 22),
(13, 12, '1', '1', 1, 1, 'CE', 1, 5, 22, '2015-07-02', 20);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_venue_master`
--

CREATE TABLE IF NOT EXISTS `tbl_venue_master` (
  `venue_id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_name` varchar(5) NOT NULL,
  `venue_type` varchar(9) NOT NULL,
  `dept_id` int(11) NOT NULL,
  PRIMARY KEY (`venue_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_venue_master`
--

INSERT INTO `tbl_venue_master` (`venue_id`, `venue_name`, `venue_type`, `dept_id`) VALUES
(5, '102', 'Lab', 1),
(7, '104', 'Lecture', 1),
(8, 'B1', 'Lab', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
