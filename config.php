<?php
error_reporting(E_ALL ^ E_NOTICE);
ob_start();

define("SITE_URL","http://".$_SERVER['SERVER_NAME']."/adminpanel/");
define("ADMIN_URL","http://".$_SERVER['SERVER_NAME']."/adminpanel/controlpanel/");

define("SITE_PATH",$_SERVER['DOCUMENT_ROOT']."/adminpanel/",true);
define("ADMIN_PATH",$_SERVER['DOCUMENT_ROOT']."/adminpanel/controlpanel/",true);

define("_BADLOGIN","Invalid User Name or Password");

define("IMG_URL",SITE_URL."images/");
define("IMG_PATH",SITE_PATH."images/",true);

define("PROFILE_IMAGE_URL",SITE_URL."profile_images/");
define("PROFILE_IMAGE_PATH",SITE_PATH."profile_images/",true);


define("TITLE","Admin Panel");

//Local Database connection Information
 $dbHost = "localhost";
 $dbName = "adminpanel";
 $dbUser = "root";
 $dbPass = "";

require_once(ADMIN_PATH."include/db.class.php");
require_once(ADMIN_PATH."include/functions.php");

//database connection instance
$db = new Database();

?>