<?php 
	require_once("../config.php");
	session_start();
?>
<html lang="HTML5"><head>
		<title>LDRP Profile Access</title>
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
     	<script src="js/index.js" type="text/javascript"></script>
	</head>
	<body id="body" style="" class="body_class">
   <div id="after_login_panel" class="back_page_blur " 
   			style=" <?php if(isset($_SESSION['confirm'])){if($_SESSION['confirm']!=""){echo "-webkit-filter: blur(0px)";}} ?>">
 
 		<?php
			require_once("header.php");
		?>
        
        <div id="conent_part" class="content_part_thumbnails_div ">
 		       	<div style="float:left" onClick="document.location='work_process.php?work=view_pro';">
                		<img class="icon_thumb_hover" id="view_profile_thumb" src="images/user.png" title="View Profile" alt="View Profile" width="130">
						<center><p class="thumbnail_text">View Profile</p></center>
                </div>
 		       	<div class="thumbnail_css"  onClick="document.location='work_process.php?work=edit_pro';">
                		<img class="icon_thumb_hover" id="edit_profile_thumb" src="images/edit.png" width="130" title="Edit Profile" alt="Edit Profile">
                		<center><p class="thumbnail_text">Edit Profile</p></center>
                </div>
 		       	<div class="thumbnail_css" onClick="document.location='work_process.php?work=leave_bal';">
                		<img class="icon_thumb_hover" id="edit_profile_thumb" src="images/leave_balance.png" width="130" title="Leave Balance" alt="Leave Balance">
                		<center><p class="thumbnail_text">Leave Balance</p></center>
                </div>
 		       	<div class="thumbnail_css" onClick="document.location='work_process.php?work=ask_leave';">
                		<img class="icon_thumb_hover" id="edit_profile_thumb" src="images/ask_leave.png" width="130" alt="Ask for a Leave" title="Ask for a Leave">
                		<center><p class="thumbnail_text">Ask for a Leave</p></center>
                </div>
				<?php if($_SESSION["role"]==2 || $_SESSION["role"]==3 || $_SESSION["role"]==5) { ?>
				<div class="thumbnail_css" onClick="document.location='work_process.php?work=view_load_taken';">
                		<img class="icon_thumb_hover" id="edit_profile_thumb" src="images/ask_leave.png" width="130" alt="Ask for a Leave" title="Ask for a Leave">
                		<center><p class="thumbnail_text">View assigned load</p></center>
                </div>
				<?php } else { ?>
				<div class="thumbnail_css" onClick="document.location='work_process.php?work=approve_leave';">
                		<img class="icon_thumb_hover" id="edit_profile_thumb" src="images/ask_leave.png" width="130" alt="Approve Leave" title="Approve Leave">
                		<center><p class="thumbnail_text">Approve Leave</p></center>
                </div>
				<?php } ?> 
 		       	<div class="thumbnail_css" onClick="document.location='work_process.php?work=your_history';">
                		<img class="icon_thumb_hover" id="edit_profile_thumb" src="images/history.png" width="130" alt="Your History" title="Your History">
                		<center><p class="thumbnail_text">Your History</p></center>
                </div>
 
                
                
        </div>
   </div>
</body></html>	