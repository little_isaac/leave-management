<html>
<head>
     	<script src="js/index.js" type="text/javascript"></script>
		<link href="css/developer.css" rel="stylesheet" type="text/css">
		<link href="css/index.css" rel="stylesheet" type="text/css">
		<!--<link href="css/general.css" rel="stylesheet" type="text/css">-->
		<link href="css/edit_pro.css" rel="stylesheet" type="text/css">
		<link href="css/view_profile.css" rel="stylesheet" type="text/css"></head>
<body>
<?php
	require_once '../config.php';
	
	if (!isUserLoggedIn())
		header("location:index.php");
	
	include("functions.php");
	
	
	
		if(isset($_SESSION['confirm']))
		{	
			if($_SESSION['confirm']!="")
			{
				$name=select_method("first_name,last_name","tbl_faculty","staff_id=".$_SESSION['confirm'].";");
			}
		}
		else
		{
			$name=Array('first_name'=>"",'last_name'=>"");
		}
	
/*if(isset($_GET['work']))
{
	if($_GET['work']=="view_pro" || $_GET['work']=="edit_pro" || $_GET['work']=="leave_bal" || $_GET['work']=="ask_leave" || $_GET['work']=="your_history")
	{
				
	}
	else
	{ header("location:index.php");die();}
}*/
?>
  		<header class="header_css" id="web_header">
       		<a href="http://ksvuniversity.org.in/" target="_blank">
            		<img class="ksv_logo" src="images/ksv.png" width="90" alt="KSV Logo" title="KADI SARVA VISHWAVIDYALAYA"></a>
       		<a href="http://www.ldrp.ac.in/" target="_blank">
            	<img class="ldrp_logo"  src="images/ldrp.png" width="90" alt="LDRP-ITR Logo" title="LDRP-ITR"></a>
        	<p id="welcome_user_id" onMouseOver="Javascript: return welcome_user_id_hover(1);" onMouseOut="Javascript: return welcome_user_id_hover(0);" 
            		onclick="Javascript: return welcome_user_id_hover(1);" class="after_login_user_name_text" >Welcome, 
												<?php echo $name['first_name']." ".$name['last_name']; ?></p>
        	<p class="after_login_ldrp_name_text">LDRP Institute Of Technology And Research</p>
 
 			<div id="welcome_user_hover" onMouseOver="Javascript: return welcome_user_id_hover(1);" onMouseOut="Javascript: return welcome_user_id_hover(0);" class="welcome_user_main_div">
            		<Div id="welcome_user_hover1" class="welcome_user_main_sign_out_div">
  						<table  width="100%">
                        	<tr onClick="Javascript:return signing_out();">
                            		<td>	<img src="images/power.png" width="28" class="sign_out_image" > 
                							<p class="sign_out_text text_hover" onClick="document.location='sign_out.php'">Sign Out</p>
                        					<hr class="after_sign_out_hr_css">
                                    </td>
                            </tr>  
  							<tr>
                            		<td>
                                    		<img src="images/power.png" width="28" class="sign_out_image"> 
                							<p class="sign_out_text text_hover">Options</p>
           							</td>
                            </tr>
                        </table> 
                   </Div>
            </div>
       </header>
              <img src="images/developer.png" 
              		onClick="Javascript:return website_dev(<?php 
																if(isset($_GET['work'])){
	if($_GET['work']=="view_pro" || $_GET['work']=="edit_pro" || $_GET['work']=="leave_bal" || $_GET['work']=="ask_leave" || $_GET['work']=="your_history")
																		{echo "2";}
																	}
																	else{echo "1";}  ?>);" 
              
              width="35" class="developer_image_css" title="Website Developer" id="developer_img">  
  
  
  
    <div id="website_develop" style="-webkit-filter: blur(0px);border:none;" class="edit_change_pro_pic_new_upper_div ">
 				<div class="edit_change_pro_pic_new_upper_inner_change_pic_div developer_div">
                <input type="image" src="images/cancel.PNG" value="Cancel" 
                			onClick="Javascript: document.location='<?php if(isset($_GET['work'])){echo "?work=".$_GET['work'];}else{echo "index.php";} ?>';"
                        		class="developer_cancel_image close_develop_images_hover" title="Close Developer Section"/>
               	<table width="100%">
                		<tr><td id="guid_td" class="website_developer_heading" colspan="2">Website Developers</td></tr>
                		<tr><td id="guid_td" class="view_profile_td_column_heading" colspan="2">Guided By</td></tr>
                </table>
                    <table width="100%;">
                    	<tr>
                        	<td><img src="images/user.png" width="180" class="img_guided_by"></td>
                            <td><img src="images/user.png" width="180" class="img_guided_by"></td>
                         </tr>
                         <tr>
                         	<td><center><p class="faculy_name">Vishal Barot</p></center></td>
                         	<td><center><p class="faculy_name">Jashvant Dave</p></center></td>
                         </tr>
                         <tr>
                         	<td><center><p class="faculy_name">Faculty</p></center></td>
                         	<td><center><p class="faculy_name">Faculty</p></center></td>
                         </tr>
                         <tr>
                         	<td><center><p class="faculy_name">LDRP-ITR</p></center></td>
                         	<td><center><p class="faculy_name">LDRP-ITR</p></center></td>
                         </tr>
                    </table>	
                    <br><br>
                	<table width="100%">
                    <tr><td class="view_profile_td_column_heading" colspan="2">Assistance By</td></tr></table>
                    <table width="100%;">
                    	<tr>
                        	<td><img src="images/user.png" width="180" class="img_guided_by"></td>
                            <td><img src="images/user.png" width="180" class="img_guided_by"></td>
                         </tr>
                         <tr>
                         	<td><center><p class="faculy_name">Gaurav Dalmiya</p></center></td>
                         	<td><center><p class="faculy_name">Milind Patel</p></center></td>
                         </tr>
                         <tr>
                         	<td><center><p class="faculy_name">Student</p></center></td>
                         	<td><center><p class="faculy_name">Student</p></center></td>
                         </tr>
                         <tr>
                         	<td><center><p class="faculy_name">LDRP-ITR</p></center></td>
                         	<td><center><p class="faculy_name">LDRP-ITR</p></center></td>
                         </tr>
                    </table>	<br><br>
                </div>
    </div>

</body>
</html>