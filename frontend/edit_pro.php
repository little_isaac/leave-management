<html>
<head>
<link href="css/edit_pro.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/edit_pro.js"></script>
</head>
<body>		<div  id="conent_part" class="content_part_thumbnails_div edit_content_main_div">
 			<table style="margin-bottom:20px;width:100%;box-shadow:5px 5px 10px #000000">
            		<tr>
                    		<td class="view_profile_td_column_heading">Edit Your Profile</td>
                    </tr>
            </table>
 			<center>
      		       	<div >
                		<img class="edit_profile_thumb" id="view_profile_thumb" src="<?php if($row['image']!=""){echo PROFILE_IMAGE_URL."th-".$row["image"];
						} else {echo "images/user.png";}?>"
                         title="Profile Picture" alt="View Profile" >
                         <p title="Click To Change Profile Picture"  class="edit_profile_thumb_change_profile" onClick="Javascript:return edit_pro_profile_change();">Change Profile</p>
      		        </div>
       		</center>     
  
  
  <!-- Showing Rest Of the Details.... -->
  
  				<form action="update_profile_details.php" method="post" name="form_profile_detail_edit" draggable="false" >
              			<table style="margin-left:10%;" >
                        	<tr><td> <br><br>  		<p class="input_edit_type_td">First Name:</p></td>
                            	<td><br><br>
		                        <input type="text" value="<?php echo $row['first_name']; ?>" required
                                	class="input_edit_type_input" name="f_name" pattern="[a-zA-Z]+" title="Only Characters Are Allowed In First Name..." ><br>
        						</td></tr>
                                <tr><td>
                		<p class="input_edit_type_td">Middle Name:</p><br></td>
<td>                        <input type="text" value="<?php echo $row['middle_name']; ?>" required
									class="input_edit_type_input" name="m_name" pattern="[a-zA-Z]+" title="Only Characters Are Allowed In Middle Name...">
</td></tr>
<tr><td>                		<p class="input_edit_type_td">Last Name:</p><br></td>
<td>                        <input type="text" value="<?php echo $row['last_name']; ?>" required
									class="input_edit_type_input" name="l_name" pattern="[a-zA-Z]+" title="Only Characters Are Allowed In Last Name..."></td>
</tr>
<br>

<tr><td>                		<p class="input_edit_type_td">Staff Login ID:</p><br></td>
<td>                        <input type="text" value="<?php echo $row['staff_login_id']; ?>" 
									class="input_edit_type_input" name="staff_login_id"  disabled="disabled"></td>
</tr>

<tr><td>                		<p class="input_edit_type_td">Primary Contact Number:</p><br></td>
<td>                        <input type="text" value="<?php echo $row['contact_1']; ?>" required
								class="input_edit_type_input" name="contact_1" pattern="[7|8|9][0-9]{9}" 
                                			title="Phone Number Starting With 7,8,9 is allowed upto 10 Digits..." ></td>
</tr>

<tr><td>                		<p class="input_edit_type_td">Secondary Contact Number:</p><br></td>
<td>                        <input  type="text" value="<?php echo $row['contact_2']; ?>" 
								class="input_edit_type_input" name="contact_2" pattern="[7|8|9][0-9]{9}" 
                                		title="Phone Number Starting With 7,8,9 is allowed upto 10 Digits..."></td>
</tr>

<tr><td>                		<p class="input_edit_type_td">Alternative Email ID:</p><br></td>
<td>                        <input type="email" value="<?php echo $row['alternative_email']; ?>" 
							class="input_edit_type_input" name="alternative_email_id" title="Please Write Correct Email ID Again..."></td>
</tr>

<tr><td>                		<p class="input_edit_type_td">Password:</p><br></td>
<td>                        <input  type="text" value="<?php echo $row['password']; ?>" required
								class="input_edit_type_input" name="password" pattern=".{5,}" 
                        		title="Please Enter Minimum Of 6 Letters..."></td>
</tr>

<tr><td>                		<p class="input_edit_type_td">Address:</p><br></td>
<td>                        <textarea required class="input_edit_type_input input_edit_type_textarea" name="address">
									<?php echo $row['address']; ?>
							</textarea></td>
</tr>


    <?php
		
						$out=mysql_query("select dept_name from tbl_department_master where dept_id=".$row['dept_id']);
						if($dept_id_db=mysql_fetch_array($out))
						{
		?>
                	<tr>
 <td>                		<p class="input_edit_type_td">Department Name:</p><br></td>
<td>                        <input type="text" value="<?php echo $dept_id_db['dept_name']; ?>" 
								class="input_edit_type_input" name="dept_name" disabled="disabled"></td>
</tr>                    <?php } 
						$out=mysql_query("select desig_name from tbl_designation_master where desig_id=".$row['designation_id']);
						if($desig_id_db=mysql_fetch_array($out))
						{
					?>
                	<tr>
    <td>                		<p class="input_edit_type_td">Designation Name:</p><br></td>
 <td>                        <input type="text" value="<?php echo $desig_id_db['desig_name']; ?>" 
 								class="input_edit_type_input" name="desig_name" disabled="disabled"></td>
                    </tr>
                    <?php
						}
					?>
    


<tr><td>                		<p class="input_edit_type_td_dob">Date of Birth:
							</td>
<td>                        <input type="date" value="<?php echo $row['dob']; ?>" required
								 class="input_edit_type_input" name="dob"></td>
</tr>


<tr><td>                		<p class="input_edit_type_td">Date of Joining:</p><br></td>
<td>                        <input  type="text" value="<?php echo date("d M Y",strtotime($row['dob'])); ?>"
								 class="input_edit_type_input" name="doj" disabled="disabled"></td>
</tr>


<tr><td>                		<p class="input_edit_type_td">Gender:</p><br></td>
<td>
                        <select name="gender" class="input_edit_type_select" 
                        	onChange="this.style.color='white';"  onFocus="this.style.color='black';" onBlur="this.style.color='white';">
                        	<option value="Male" <?php if($row['gender']=="male" || $row['gender']=="Male" || $row['gender']=="MALE"){echo "selected	";} ?>>
                            				Male</option>
                            <option value="Female"  <?php if($row['gender']=="female" || $row['gender']=="Female" || $row['gender']=="FEMALE"){echo "selected";}?>>
                            				Female</option>
                        </select>
                        
                        </td>
</tr>
<tr><td></td><td> <input type="image" src="images/save.png" width="45" height="45" title="Save Changes" value="Save Changes" alt="Save Changes" style="background-color:#3c3d4b;margin-left:22px;"
						class="cancel_submit_images_hover">
				<img src="images/cancel.png" onClick="document.location='index.php';" class="input_edit_type_cancel_button cancel_submit_images_hover"
                 title="Cancel Editing" alt="Cancel Editing" style="background-color:#3c3d4b;" width="45" height="45">
</td></tr>





</table>             </form>         
        
        
    </div>
    
    <div id="new_upper_pro_pic_change_div" style="-webkit-filter: blur(0px);border:none;" class="edit_change_pro_pic_new_upper_div">
 				<div class="edit_change_pro_pic_new_upper_inner_change_pic_div">
                	<table width="100%"><tr><td class="view_profile_td_column_heading" colspan="2">Change Profile Picture</td></tr></table>
                		
						<table>
						<tr>
							<td rowspan="4">
						<img  class="profile_thumb_change_pro_pic" id="view_profile_thumb_1" src="<?php if($row['image']!=""){echo PROFILE_IMAGE_URL."th-".$row["image"];
						} else {echo "images/user.png";}?>" width="400px" height="300px"
                         title="Profile Picture" alt="Profile Picture">
                    		</td>
						</tr>
						<tr>
							<td>
							<img  class="profile_thumb_change_pro_pic" id="view_profile_thumb_2" src="" width="400px" height="300px"
	                     title="Profile Picture" name="view_profile_thumb_2" alt="Changed Profile Picture" style="display:none;" >
                      <p class="pro_pic_change_div_user_name"><?php echo $row['first_name']." ".$row['last_name']; ?></p>
							</td>
						</tr>
						<tr>
							<td>
								<p class="pro_pic_change_div_age">Age: <?php echo (date('Y',time())- date("Y",strtotime($row['dob'])))." Years"; ?> </p>		
							</td>
						</tr>
						<tr>
							<td>
								<form  method="POST" action="update_profile_picture.php" enctype="multipart/form-data">
   					<input type="file" name="chang_pro_pic_file" id="chang_pro_pic_file" onChange="Javascript:return pro_pic_input_change();"
                    	class="pro_pic_change_div_file_input "/>
                        
                        <input type="hidden" value="<?php echo $row['image']; ?>" name="image_name" />
                        <input type="hidden" value="<?php if(isset($_GET['work'])){if($_GET['work']=="edit_pro"){echo "edit_pro";}} ?>" name="current_page" />
                       
                        <input type="submit" value="Change Profile Picture" id="pro_update_pic_submit" name="submit" disabled="disabled" 
                        	class="cancel_submit_images_hover pro_pic_change_div_submit_button_input button" />
                        <input type="button" value="Cancel" onClick="Javascript: document.location='?work=edit_pro';"
                        		class="pro_pic_change_div_cancel_button_input cancel_submit_images_hover button" />
                        </form>
							</td>
						</tr>
						</table>
						
                         
                      
   
   			
                </div>
    </div>
</body></html>